A large list of reasons why to avoid Sails.js and Waterline: https://kev.inburke.com/kevin/dont-use-sails-or-waterline/

---

__UPDATE:__ The issue has been [reopened](https://github.com/balderdashy/sails/issues/2830#issuecomment-140794914) by the founder of Balderdash. Mind that the below was written back when this was not the case yet, and judge appropriately.

---

Furthermore, the CEO of Balderdash, the company behind Sails.js, stated the following:

> > "we promise to push a fix within 60 days",

> @kevinburkeshyp This would amount to a Service Level Agreement with the entire world; this is generally not possible, and does not exist in any software project that I know of.

Upon notifying him in the thread that I actually offer [exactly that guarantee](http://cryto.net/~joepie91/), and that his statement was thus incorrect, he accused me of "starting a flamewar", and proceeded to [delete my posts](https://github.com/balderdashy/sails/issues/2830).

He is apparently also unaware that Google Project Zero expects the exact same - a hard deadline of 90 days, after which an issue is publicly disclosed.

Now, just locking the thread would have been at least somewhat justifiable - he might have legitimately misconstrued my statement as inciting a flamewar.

What is __not__ excusable, however, is removing my posts that show his (negligent) statement is wrong. This raises serious questions about what the Sails maintainers consider more important: their reputation, or the actual security of their users.

It would have been perfectly possible to just leave the posts intact - the thread would be locked, so a flamewar would not have been a possibility, and each reader could make up their own mind about the state of things.

In short: __Avoid Sails.js. They do not have your best interests at heart, and this could result in serious security issues for your project.__

For reference, the full thread is below, pre-deletion.

![](http://cryto.net/~joepie91/sails.png)